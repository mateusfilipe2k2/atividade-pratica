## Table of contents 

-  [Atividade Prática Git TerraLab](#atividade-pr%C3%A1tica-git-terralab)
	*  [1ª e 2ª Questão](#1%C2%AA-e-2%C2%AA-quest%C3%A3o)
	*  [3ª Questão](#3%C2%AA-quest%C3%A3o)
		+  [Execução](#execu%C3%A7%C3%A3o)
	*  [4ª Questão](#4%C2%AA-quest%C3%A3o)
	*  [5ª Questão](#5%C2%AA-quest%C3%A3o)
	*  [6ª Questão](#6%C2%AA-quest%C3%A3o)
	*  [7ª Questão](#7%C2%AA-quest%C3%A3o)
	*  [8ª Questão](#8%C2%AA-quest%C3%A3o)
	*  [9ª Questão](#9%C2%AA-quest%C3%A3o)
	*  [10ª Questão](#10%C2%AA-quest%C3%A3o)


# Atividade Prática Git TerraLab

Este projeto tem como objetivo praticar os conceitos aprendidos sobre git e GitLab
na primeira semana do processo seletivo do TerraLab

Em seguida serão colocadas cada situação de acordo com o que foi pedido no
[enunciado](https://docs.google.com/document/d/1dtEllYa7skoRc4Iada_qNLGBCHEQURHsip-RP36B9cQ/edit#) da tarefa.

O [link](https://gitlab.com/mateusfilipe2k2/atividade-pratica/-/issues/1) para o repositório remoto no gitLab se encontra aqui.

## 1ª e 2ª Questão

Referente e **1º questão**, foi criado uma conta no GitLab e o repositório no gitLab. Este arquivo README foi lançado como o commit inicial, referene a **2ª questão**, alem de ir sendo atualizado com cada commit explicando cada questão. 

## 3ª Questão

```
ENUNCIADO: 

Crie nesse repositório um arquivo que vai se chamar calculadora.js, abra 
esse arquivo em seu editor de códigos favoritos e adicione o seguinte código:
Descubra o que esse código faz através de pesquisas na internet, também
descubra como executar um código em javascript e dado que o nome do nosso 
arquivo é calculadora.js e você entendeu o que o código faz, escreva abaixo 
como executar esse código em seu terminal:

```

```javascript
  const args = process.argv.slice(2);
  console.log(parseInt(args[0]) + parseInt(args[1]));
```

Esse código em javaScript, pega dois números passados por parâmetro na execução do código e faz a soma e impressão do resultado desses dois números.

### Execução
Para Executar o código basta ter o [node](https://nodejs.org/en/download/) devidamente configurado na maquina e então executar:

```javascript
  $ node calculadora.js 1 2
  $ 3
```

Lembrando de passar dois valore numéricos inteiros por parâmetro, no caso o resultado da soma de 1 + 2 = 3

## 4ª Questão

```
ENUNCIADO: 

Agora que você já tem um código feito e a resposta aqui, você precisa subir isso para seu repositório. 
Sem usar git add . descubra como adicionar apenas um arquivo ao seu histórico de commit e adicione calculadora.js a ele.

Que tipo de commit esse código deve ter de acordo ao conventional commit.
Que tipo de commit o seu README.md deve contar de acordo ao conventional commit. 
Por fim, faça um push desse commit.

```

Usamos o camando abaixo para adicionar somente um arquivo para a staging area:
```bash
  $ git add calculadora.js
```

O arquivo vai ser adicionado para a staging area e poderá ser commitado. 
De acordo o Conventional Commits a mensagem do commit deverá ser algo do tipo:
```bash
  $ git commit -m "feat: permite calculo da soma de dois números"
```
Foi feito o push do novo commit para o repositório remoto

O commit do README.md pode ser feito da seguinte forma, seguindo o padrão do Conventional Commits:
```bash
  $ git commit -m "docs: atualização inicial da documentação"
```

## 5ª Questão

```
ENUNCIADO: 
Copie e cole o código abaixo em sua calculadora.js:

Descubra o que essa mudança representa em relação ao 
conventional commit e faça o devido commit dessa mudança. 
```

```javascript
  const soma = () => {
    console.log(parseInt(args[0]) + parseInt(args[1]));
  };

  const args = process.argv.slice(2);

  soma();
```

De acordo com o Conventional Commits a mudança acima pode ser descrita como um refactor.
Então o commit dessa mudança pode ser feito da seguinte maneira:

```bash
  $ git commit -m "refactor: mudança no backend da calculadora. Adicionada uma função anónima"
```

Por fim, a atualização do README.md também foi feita.

## 6ª Questão

```
ENUNCIADO: 
João entrou em seu repositório e o deixou da seguinte maneira:

Depois disso, realizou um git add . 
e um commit com a mensagem: "Feature: added subtraction"
faça como ele e descubra como executar o seu novo código.

Nesse código, temos um pequeno erro, encontre-o e corrija 
para que a soma e divisão funcionem.n

Por fim, commit sua mudança. 

```

Foi feito o "git add ." e um commit com a mensagem: "Feature: added subtraction" 
referente ao código adicionado.

Este código tem um erro com relação aos índices 
do args dentro das funções soma e sub, no lugar de args[0] deve ser args[1]


```javascript
const soma = () => {
    console.log(parseInt(args[0]) + parseInt(args[1]));
};

const sub = () => {
    console.log(parseInt(args[0]) - parseInt(args[1]));  
}

const args = process.argv.slice(2);

switch (args[0]) {
    case 'soma':
        soma();
    break;

    case 'sub':
        sub();
    break;

    default:
        console.log('does not support', arg[0]);
}
```

O commit dessa mudança foi feita com:

```bash
  $ git commit -m "fix: correção de erro no indice dos argumentos passado para a calculadora"
```

Por fim foi atualizada a documentação.

## 7ª Questão 

```
ENUNCIADO: 

Por causa de joãozinho, você foi obrigado a fazer correções
na sua branch principal! O produto foi pro saco e a empresa perdeu
muito dinheiro porque não conseguiu fazer as suas contas, graças a isso
o seu chefe ficou bem bravo e mandou você dar um jeito disso nunca acontecer.

Aprenda a criar uma branch, e desenvolva a feature de divisão nessa branch.


```

Foi criado a branch "develop" e a partir dela foi criada a branch "1-adicionar-funcao-de-divisao"
para a devida implementação da nova funcionalidade.

```bash
  $ git commit -m "feat: adicionado função de divisão com verificação
```

Criação de Issue para implementação da nova feature
![Criação de Issue para implementação da nova feature](https://i.imgur.com/hrF6iUe.png)

## 8ª Questão 

```
ENUNCIADO: 

Agora que a sua divisão está funcionando e você garantiu que
não afetou as outras funções, você está apto a fazer um merge request
Em seu gitlab, descubra como realizá-lo de acordo com o gitflow.

```

Com a divisão implementada na branch "1-adicionar-funcao-de-divisao" 
Podemos partir para a branch staging onde serão testadas as modificações e sua interação com o resto do projeito.

Depoois disso podemos fazer o merge request da staging com o master. O que Foi feito

![Merge Request](https://i.imgur.com/OZKFLnX.png)

## 9ª Questão 

```
ENUNCIADO: 

João quis se redimir dos pecados e fez uma melhoria em seu código,
mas ainda assim, continuou fazendo um push na master, faça a seguinte alteração no código e fez o commit com a mensagem:

"refactor: calculator abstraction"

Para piorar a situação, joão não te contou como executar esse novo código, enquanto você não descobre como executá-lo lendo o código, 
e seu chefe não descobriu que tudo está comprometido, faça um revert 
através do seu gitlab para que o produto volte ao normal o quanto antes!
```

```javascript
var x = args[0];
var y = args[2];
var operator = args[1];

function evaluate(param1, param2, operator) {
  return eval(param1 + operator + param2);
}

if ( console.log( evaluate(x, y, operator) ) ) {}
```

O projeito foi restaurado para o commit anterior. Revertendo as mudanças do último commit.

## 10ª Questão 

```
ENUNCIADO: 

Descubra como executar esse novo código e que operações ele é capaz de realizar. Deixe sua resposta aqui, e explique o que essas funções javascript fazem. 
```

O novo código pode ser executado 

```bash
  $ node calculadora.js valor1 operador valor2
```
Sendo o operador os operadores em si na programação, como o +, -, /.

A forma como o código funciona é a seguinte: 
Argumentos passados por parâmetro na execução do programa são armazanados de acordo.
Eles são passados para a função evaluate a qual chama a função eval com a concatenação desses três argumentos, executando o código javaScript correspondente a essa concatenação.

Exemplo:
```bash
  $ node calculadora.js 1 + 2
  $ 3
```

O código executado pelo programa nesse caso vai ser, 1+2. Sendo assim o programa suporta qualquer operação a qual o java script também suportar.

